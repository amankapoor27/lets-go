package main

import (
	"fmt"
	"net/http"
)

func main() {

	url := "https://golang.org"
	result, err := getContentType(url)

	if err != nil {
		fmt.Printf("ERROR: %s \n", err)
	} else {
		fmt.Print(result)
	}
}

func getContentType(url string) (string, error) {

	resp, err := http.Get(url)

	fmt.Print(resp)

	if err != nil {
		return "", err
	}

	defer resp.Body.Close() //defer helps in garbage colllection

	contentType := resp.Header.Get("content-type")

	if contentType == "" {
		return "", fmt.Errorf("Content-type header not found")
	}

	return contentType, nil
}
