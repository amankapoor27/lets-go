package main

import (
	"fmt"
	"strings"
)

func main() {

	var sentence string = "this is a this this still it likes this likes that and this"

	mySlice := strings.Fields(sentence)

	myMap := make(map[string]int)

	for _, val := range mySlice {
		if myMap[val] == 0 {
			myMap[val] = 1
		} else {
			myMap[val] = myMap[val] + 1
		}
	}

	fmt.Println(myMap)
}
