package main

import "fmt"

type myStruct struct {
	name string
	age  int
}

func main() {

	var s myStruct

	s.name = "aman"
	s.age = 31

	s.byReference()
	fmt.Println(s.toString())

}

func (s myStruct) toString() string {

	stringified := fmt.Sprint(s.age, s.name)

	return stringified
}

func (s *myStruct) byReference() {

	s.age = 43

}
