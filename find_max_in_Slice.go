package main

import (
	"fmt"
)

func main() {

	mySlice := []int{3, 5, 1, 5, 67, 5, 0, 32}

	var max int
	for _, i := range mySlice {
		fmt.Print("  ", i)
		if max < i {
			max = i
		}
	}

	fmt.Print("\n\nMax:  ", max)
}
